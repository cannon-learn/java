package com.qihoo.mall.thread.futureTask;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class A2 implements Callable{
	private String name;
	public A2(String name) {
		this.name=name;
	}
	@Override
	public String call() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "a";
	}
	
	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(2);
		FutureTask<String> ft=new FutureTask(new A2("A"));
		Future fu=pool.submit(ft);
		try {
			String rst = ft.get();
			fu.cancel(true);
			ft.cancel(true);
			System.out.println(rst);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		pool.shutdown();
	}
	
}
