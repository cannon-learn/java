package com.qihoo.mall.thread.cyclicBarrier;

import java.util.Date;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class A implements Runnable{
	private String name;
	private int wait;
	private CyclicBarrier cb; 
	public A(CyclicBarrier  cb,String name,int wait) {
		this.cb=cb;
		this.name=name;
		this.wait=wait;
	}
	@Override
	public void run() {
		try {
			Thread.sleep(wait*1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("before-"+name+"-"+new Date());
		try {
			cb.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
		System.out.println("after-"+name+"-"+new Date());
	}
	
	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(6);
		CyclicBarrier cb=new CyclicBarrier(3);
		pool.submit(new A(cb,"thread1",1));
		pool.submit(new A(cb,"thread2",2));
		pool.submit(new A(cb,"thread3",3));
		pool.submit(new A(cb,"thread4",4));
		pool.submit(new A(cb,"thread5",5));
		pool.submit(new A(cb,"thread6",6));
		
		pool.shutdown();
	}
}
