package com.qihoo.mall.thread.cas;

import java.util.concurrent.locks.ReentrantLock;

public class TestVolatile implements Runnable{
	public volatile int i;
	@Override
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		i=1;
		System.out.println("thread run over");
	}
	public static void main(String[] args) {
		TestVolatile a=new TestVolatile();
		new Thread(a).start();
		while(true) {
			if(a.i==1) {
				System.out.println("i==1");
			}
		}
	}
}
