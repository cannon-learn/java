package com.qihoo.mall.thread.cas;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

public class TestCas implements Runnable{
	private AtomicInteger  ai;
	private AtomicReference<String> ar;
	public TestCas(AtomicInteger ai) {
		this.ai=ai;
	}
	@Override
	public void run() {
//		boolean succ = ai.compareAndSet(0, 1);
//		System.out.println(succ);
		boolean succ=ar.compareAndSet("1", "2");
		System.out.println(succ);
	}
	public static void main(String[] args) {
		
		AtomicInteger ai=new AtomicInteger();
		AtomicReference<String> ar = new AtomicReference<String>("1");
		String rst=ar.get();
//		int rst = ai.get();
		System.out.println("init is "+rst);
		TestCas testCas1 = new TestCas(ai);
		TestCas testCas2 = new TestCas(ai);
		testCas1.setAr(ar);
		testCas2.setAr(ar);
		new Thread(testCas1).start();
		new Thread(testCas2).start();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		rst=ai.get();
		rst=ar.get();
		System.out.println("last is "+rst);
	}
	public AtomicReference getAr() {
		return ar;
	}
	public void setAr(AtomicReference ar) {
		this.ar = ar;
	}
}
