package com.qihoo.mall.thread.map;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class TestMap implements Runnable{
	private Map<String,String> map;
	private int start;
	private CountDownLatch cl;
	public TestMap(Map map,int start,CountDownLatch cl) {
		this.map=map;
		this.start=start;
		this.cl=cl;
	}
	@Override
	public void run() {
		for(int i=0;i<10000;i++) {
			map.put(i+start+"", i+start+1+"");
		}
		try {
			synchronized(CountDownLatch.class) {
				cl.countDown();
			}
			cl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
//		Map<String,String> map=new HashMap<>();//线程不安全
		Map<String,String> map=new ConcurrentHashMap<>();
		CountDownLatch cl=new CountDownLatch(6);
		for(int i=0;i<5;i++) {
			new Thread(new TestMap(map,i*10000,cl)).start();
		}                                                                  

		try {
			synchronized(CountDownLatch.class) {
				cl.countDown();
			}
			cl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		for(int i=0;i<50000;i++) {
			String rst = map.get(i+"");
			if(rst==null) {
				System.out.println(i+" is null");
				continue;
			}
			if(rst.equals(i+1+"")) {
				continue;
			}else {
				System.out.println(i+" is "+rst);
			}
		}
	}
	
}
