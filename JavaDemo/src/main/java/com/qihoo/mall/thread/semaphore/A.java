package com.qihoo.mall.thread.semaphore;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class A implements Runnable{
	private Semaphore sem;
	private String name;
	private int waitTime;
	public A(Semaphore sem,String name,int waitTime) {
		this.sem=sem;
		this.name=name;
		this.waitTime=waitTime;
	}
	@Override
	public void run() {
		try {
			Thread.sleep(waitTime*1000);
			System.out.println(name+"-before acquire-"+new Date());
			sem.acquire();
			System.out.println(name+"-after acquire-"+new Date());
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sem.release();
	}
	
	public static void main(String[] args) {
		Semaphore sem=new Semaphore(3);
		ExecutorService pool = Executors.newFixedThreadPool(6);
		pool.submit(new A(sem,"thread1",1));
		pool.submit(new A(sem,"thread2",2));
		pool.submit(new A(sem,"thread3",3));
		pool.submit(new A(sem,"thread4",4));
		pool.submit(new A(sem,"thread5",5));
		pool.submit(new A(sem,"thread6",6));
		
//		pool.shutdown();
	}
	
}
