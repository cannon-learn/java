package com.qihoo.mall.thread.exchanger;

import java.util.Date;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class A implements Runnable{
	private Exchanger<String> exchanger;
	private String name;
	public A(Exchanger<String> exchanger,String name) {
		this.exchanger=exchanger;
		this.name=name;
	}
	@Override
	public void run() {
		try {
			System.out.println(name+"-"+new Date());
			String rst = this.exchanger.exchange(name);
			System.out.println(name+"-"+rst+"-"+new Date());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(2);
		Exchanger<String> ex=new Exchanger();
		pool.submit(new A(ex,"A"));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pool.submit(new A(ex,"B"));
		pool.shutdown();
	}
	
}
